use csv::ReaderBuilder;
use serde::{Deserialize, Serialize};
use serde_json::json;
use qdrant_client::prelude::*;
use qdrant_client::qdrant::vectors_config::Config;
use qdrant_client::qdrant::{
    CreateCollection, VectorParams, VectorsConfig,
};



#[derive(Debug, Deserialize)]
struct Bacteria {
    #[serde(rename = "Name")]
    name: String,
    #[serde(rename = "Family")]
    family: String,
    #[serde(rename = "Where Found")]
    where_found: String,
    #[serde(rename = "Harmful to Humans")]
    harmful_to_humans: String,
}

#[derive(Serialize)]
struct QdrantData {
    name: String,
    family: String,
    where_found: String,
    harmful_to_humans: bool,
}

async fn ingest_data(filename: &str) -> Result<(), Box<dyn std::error::Error>> {
    // Read CSV file
    let mut reader = ReaderBuilder::new().has_headers(true).from_path(filename)?;

    // Connect to Qdrant database
    //let client = reqwest::Client::new();
    let client = QdrantClient::from_url("http://localhost:6334").build()?;
    //let endpoint = "http://localhost:6333/collections/default/documents";
    let collection_name = "test";
    client.delete_collection(collection_name).await?;

    client
        .create_collection(&CreateCollection {
            collection_name: collection_name.into(),
            vectors_config: Some(VectorsConfig {
                config: Some(Config::Params(VectorParams {
                    size: 10,
                    distance: Distance::Cosine.into(),
                    ..Default::default()
                })),
            }),
            ..Default::default()
        })
        .await?;
    
    // Iterate over CSV records
    for (id, result) in reader.deserialize().enumerate() {
        let record: Bacteria = result?;
        
        // Map CSV record to Qdrant data structure
        let qdrant_data = QdrantData {
            name: record.name,
            family: record.family,
            where_found: record.where_found,
            harmful_to_humans: record.harmful_to_humans.to_lowercase() == "yes",
        };
        
        // Insert data into Qdrant
        // let response = client.post(endpoint)
        //     .json(&json!([qdrant_data]))
        //     .send()
        //     .await?;
        // println!("Response status: {}", response.status());
        let payload: Payload = json!(
            qdrant_data
        )
            .try_into()
            .unwrap();
        let points = vec![PointStruct::new(id as u64, vec![12.; 10], payload)];
        client
        .upsert_points_blocking(collection_name, None, points, None)
        .await?;
        // if !response.status().is_success() {
        //     eprintln!("Failed to insert data for");
        // }
    }
    
    Ok(())
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    ingest_data("bacteria.csv").await?;
    println!("Data ingestion successful!");
    Ok(())
}
