# Data Processing with Vector Database


This repository contains Rust Lambda functions for ingesting data into a Qdrant Vector database and querying the database. The Vector database is a high-performance database optimized for handling vectorized data, making it suitable for various use cases, including machine learning, similarity search, and more.

## Project Structure
The project consists of two main components:

* Data Ingestion: Lambda function responsible for ingesting data from a CSV file into the Vector database.
* Database Query: Lambda function for querying the Vector database based on user-provided criteria.

### Requirements
1. Rust installed on your system. You can install Rust by following the instructions on rust-lang.org.
2. Docker running with the Vector database container.

### Data
The dataset used in this project was found on [Kaggle](https://www.kaggle.com/datasets/kanchana1990/bacteria-dataset). It contains information about various types of bacteria, including their names, families, habitats, and whether they are harmful to humans. Here's a brief overview of the data fields:

1. Name: The name of the bacteria species.
2. Family: The taxonomic family to which the bacteria belongs.
3. Where Found: The natural habitat or environment where the bacteria are commonly found.
4. Harmful to Humans: Indicates whether the bacteria species is harmful to humans. This field typically contains binary values like "Yes" or "No".

## Data Ingestion
The data ingestion Lambda function reads data from a CSV file and inserts it into the Vector database. Here's how to use it:

* Setup Environment: Ensure that Docker is running with the Vector database container. 
```
# With env variable
docker run -p 6333:6333 -p 6334:6334 \
    -e QDRANT__SERVICE__GRPC_PORT="6334" \
    qdrant/qdrant
```
* Prepare Data: Create a CSV file containing the data you want to ingest into the Vector database. The CSV file should have the required fields and structure.
* Configure Function: Update the ingest_data function in the data_ingestion directory with the appropriate code to read the CSV file and insert data into the Vector database.
* Run Function: Execute the command cargo run in the terminal from the data_ingestion directory to run the data ingestion Lambda function.
* Verify: Once the function completes execution, check the Vector database to ensure that the data has been successfully ingested.

## Database Query
The database query Lambda function allows users to query the Vector database based on specific criteria. Here's how to use it:

* Setup Environment: Ensure that Docker is running with the Vector database container.
* Configure Function: Update the query_database function in the query_database directory with the appropriate code to execute queries against the Vector database.
* Run Function: Execute the command cargo run in the terminal from the query_database directory to run the database query Lambda function. It accepts any field in the database and its corresponding column as a filter.
* Invoke Function: Invoke the Lambda function with a command specifying the query criteria. For example, name:Escherichia coli.
* View Results: Once the function completes execution, review the output to see the results of the database query.

```
cargo lambda invoke --data-ascii "{ \"command\": \"name:Escherichia coli\" }"

{"req_id":"89c7cadf-3dd7-402b-ae27-1da39a72ee70","qdrant_data":{"name":"Escherichia coli","family":"Enterobacteriaceae","where_found":"Intestinal tract","harmful_to_humans":true}}
```


## Screenshots
1. Data Ingested in Qdrant Database under the test collection
![](ingest.png)
![](ingest1.png)

2. Data Visualization in Qdrant databaseed based on whether a bacteria is harmful or not
![](viz.png)

3. Test lambda function to query databse
![](test.png)