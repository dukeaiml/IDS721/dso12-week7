use serde::{Deserialize, Serialize};
use serde_json::json;
use qdrant_client::prelude::*;
//use qdrant_client::qdrant::vectors_config::Config;
use qdrant_client::qdrant::{
   Condition, Filter, SearchPoints,
};
use lambda_runtime::{run, service_fn, tracing, Error, LambdaEvent};


#[derive(Debug, Deserialize)]
struct Bacteria {
    #[serde(rename = "Name")]
    name: String,
    #[serde(rename = "Family")]
    family: String,
    #[serde(rename = "Where Found")]
    where_found: String,
    #[serde(rename = "Harmful to Humans")]
    harmful_to_humans: String,
}

#[derive(Serialize)]
struct QdrantData {
    name: String,
    family: String,
    where_found: String,
    harmful_to_humans: bool,
}

async fn query_database (event: LambdaEvent<Request>) -> Result<Response, Error> {
//async fn query_database (key: &str, value: &str) -> Result<(), Box<dyn std::error::Error>> {
    // Connect to Qdrant database
    //let client = reqwest::Client::new();
    let client = QdrantClient::from_url("http://localhost:6334").build()?;
    //let endpoint = "http://localhost:6333/collections/default/documents";
    let collection_name = "test";

    // Extract some useful info from the request
    let command = event.payload.command;
    //Parse the command to extract the key and value
    let parts: Vec<&str> = command.split(':').collect();
    if parts.len() != 2 {
        return Err("Invalid command format".into());
    }
    let key = parts[0].trim();
    let value = parts[1].trim();


    // Execute the search query
    let search_result = client
        .search_points(&SearchPoints {
            collection_name: collection_name.into(),
            vector: vec![11.; 10],
            filter: Some(Filter::all([Condition::matches(key, value.to_string())])),
            limit: 10,
            with_payload: Some(true.into()),
            ..Default::default()
        })
        .await?;

    // Print the search result
    //dbg!(&search_result);
    let qdrant_data = if let Some(scored_point) = search_result.result.get(0) {
        let harmful_to_humans = scored_point.payload["harmful_to_humans"].as_bool().unwrap_or(false);
        let family = scored_point.payload["family"].as_str().map_or_else(|| String::new(), |s| s.to_string());
        let where_found = scored_point.payload["where_found"].as_str().map_or_else(|| String::new(), |s| s.to_string());
        let name = scored_point.payload["name"].as_str().map_or_else(|| String::new(), |s| s.to_string());


        JsonResponse {
            name: name.to_string(),
            family: family.to_string(),
            where_found: where_found.to_string(),
            harmful_to_humans,
        }
    } else {
        // If no search result is found, return default values
        JsonResponse {
            name: String::new(),
            family: String::new(),
            where_found: String::new(),
            harmful_to_humans: false,
        }
    };
    
    let resp = Response {
        req_id: event.context.request_id.clone(),
        qdrant_data,
    };

    // Return `Response` (it will be serialized to JSON automatically by the runtime)
    Ok(resp)
    //Ok(())
}

// #[tokio::main]
// async fn main() -> Result<(), Box<dyn std::error::Error>> {
//     //query_database("bacteria.csv").await?;
//     //println!("Data ingestion successful!");
//     let key = "name"; // Example column name
//     let value = "Escherichia coli"; // Example value

//     query_database(key, value).await?;
//     println!("Query successful!");
//     Ok(())
// }
#[derive(Serialize)]
struct JsonResponse {
    name: String,
    family: String,
    where_found: String,
    harmful_to_humans: bool,
}

#[derive(Deserialize)]
struct Request {
    command: String,
}

#[derive(Serialize)]
struct Response {
    req_id: String,
    qdrant_data: JsonResponse,
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing::init_default_subscriber();

    run(service_fn(query_database)).await?;
    println!("Query successful!");
    Ok(())
}
